<?php
/**
 * @file
 * Drupal page callback functions with forms and other stuff to use.
 */

/**
 * Implements hook_form().
 *
 * iMprove Admin module settings form.
 */
function improve_admin_settings_form($form, &$form_state) {

  $form['site_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Custom site settings page'),
    '#collapsible' => FALSE,
  );

  $form['site_settings']['improve_admin_settings_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Setting menu link title'),
    '#default_value' => variable_get('improve_admin_settings_title'),
    '#required' => TRUE,
  );

  $form['site_settings']['improve_admin_settings_machine_name'] = array(
    '#type' => 'machine_name',
    '#default_value' => variable_get('improve_admin_settings_machine_name'),
    '#maxlength' => 32,
    '#machine_name' => array(
      'exists' => 'improve_admin_settings_machine_name_exists',
    ),
  );

  return system_settings_form($form);
}

function improve_admin_settings_machine_name_exists($value) {
  return FALSE;
}


/**
 * Implements hook_form().
 *
 * Site settings form.
 */
function improve_admin_site_settings_form($form, &$form_state) {

  $form['groups'] = array(
    '#type' => 'vertical_tabs',
  );

  return system_settings_form($form);
}

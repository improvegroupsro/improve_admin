
<?php

/**
 * @file
 * Display Suite 2 column responsive template.
 */
?>
<<?php print $layout_wrapper; print $layout_attributes; ?> class="improve-ds-2col-responsive <?php print $classes;?> clearfix">

<?php if (isset($title_suffix['contextual_links'])): ?>
  <?php print render($title_suffix['contextual_links']); ?>
<?php endif; ?>

<<?php print $header_wrapper ?> class="group-header<?php print $header_classes; ?>">
  <div class="ds-region-wrapper">
    <?php print $header; ?>
  </div>
</<?php print $header_wrapper ?>>

<<?php print $left_wrapper ?> class="group-left<?php print $left_classes; ?>">
  <div class="ds-region-wrapper">
    <?php print $left; ?>
  </div>
</<?php print $left_wrapper ?>>

<<?php print $right_wrapper ?> class="group-right<?php print $right_classes; ?>">
  <div class="ds-region-wrapper">
    <?php print $right; ?>
  </div>
</<?php print $right_wrapper ?>>

<<?php print $footer_wrapper ?> class="group-footer<?php print $footer_classes; ?>">
  <div class="ds-region-wrapper">
    <?php print $footer; ?>
  </div>
</<?php print $footer_wrapper ?>>

</<?php print $layout_wrapper ?>>

<?php if (!empty($drupal_render_children)): ?>
  <?php print $drupal_render_children ?>
<?php endif; ?>

